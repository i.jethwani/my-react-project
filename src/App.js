import './App.css';
import HomePage from './main-components/homepage/HomePage';
import NavBar from './shared-components/NavBar'
import { Route, Routes } from 'react-router-dom';
import RestaurantPage from './main-components/restaurantpage/RestaurantPage';
import restaurantContext from './context/RestaurantContext';
import { useContext, useEffect } from 'react';
import { Authentication } from './main-components/authenticate/Authentication';
import SignInForm from './main-components/authenticate/pages/SignInForm';
import SignUpForm from './main-components/authenticate/pages/SignUpForm';


function App() {
  const restaurantsCon = useContext(restaurantContext);
  const { restaurant } = restaurantsCon;
  useEffect(() => {
    window.scrollTo(0, 0);
  })
  return (
    <div className='container my-2'>

      <NavBar />
      <Routes>
        <Route exact path="/" element={<HomePage />} />
        <Route exact path="/restaurant/:id" element={<RestaurantPage restaurant={restaurant} />} />
        <Route exact path="/authenticate" element={<Authentication />}/>
        <Route path="/sign-up" element={<SignUpForm />}/>
        <Route path="/sign-in" element={<SignInForm/>} />
      </Routes>
    </div>
  );
}

export default App;
