import React, { useContext } from 'react'
import { CartContext } from '../context/CartContext';

const Modal = React.forwardRef((props, ref) => {

    const cartCon = useContext(CartContext);
    const { cartItems, setCartItems } = cartCon

    const handleOnProceed = () => {
        const filteredArray = [];
        cartItems.filter(cartItem => cartItem.menuItem.restaurant.restaurantId !== props.restaurantId).map(filteredItems => filteredArray.push(filteredItems))
        setCartItems(filteredArray);
        ref.current.click();
    }

    const handleOnCancel = (menuItem) => {
        console.log(menuItem);
        setCartItems(cartItems.filter((item) => item.menuItem.menuItemId !== menuItem.menuItemId))
    }

    return (
        <>
            {/* <!-- Button trigger modal --> */}
            <button type="button" ref={ref} className="btn btn-primary d-none" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                Launch static backdrop modal
            </button>

            {/* <!-- Modal --> */}
            {/* {console.log(props.menuItem)} */}
            <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="staticBackdropLabel">Alert !!!</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={()=>{handleOnCancel(props.menuItem)}}></button>
                        </div>
                        <div className="modal-body">
                            You are trying to add a dish from a different restaurant, which may lead to remove the previous selected dishes.
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={()=>{handleOnCancel(props.menuItem)}} >Cancel</button>
                            <button type="button" className="btn btn-primary" onClick={handleOnProceed}>Proceed</button>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
})

export default Modal
