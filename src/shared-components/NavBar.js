import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { BiLogIn } from "react-icons/bi";


const Navbar = () => {
  const navigate = useNavigate();

  const handleOnLoginClick = ()=>{
    navigate("/authenticate");
  }
  return (
    <div>
      <nav className="navbar navbar-expand-lg bg-body-tertiary" style={{borderRadius:"15px"}}>
        <div className="container-fluid">
          <Link className="navbar-brand" to="/"><h2>Direction Eats</h2></Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link active" aria-current="page" to="/">Home</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/">About</Link>
              </li>
              
            </ul>
            <form className="d-flex" role="search">
              <input className="form-control me-2" type="search" placeholder="Search for food/restaurant" aria-label="Search" />
              <button className="btn btn-outline-success" type="submit">Search</button>
            </form>
            <div><BiLogIn style={{height:"2em",width:"3em",cursor:"pointer"}} onClick={handleOnLoginClick}/></div>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default Navbar
