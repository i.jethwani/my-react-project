import React, { useContext } from 'react'
import { BsDashSquareDotted, BsPlusSquareDotted } from "react-icons/bs";
import { CartContext } from '../../context/CartContext';

const CartItemTemplate = (props) => {

    const cartCon = useContext(CartContext);
    const { cartItems, setCartItems } = cartCon

    const handleQuantityIncrease = (id) => {
        const currentMenuItemIndex = cartItems.findIndex((item) => item.menuItem.menuItemId === id);
        const updatedMenuItem = { ...cartItems[currentMenuItemIndex], itemQuantity: cartItems[currentMenuItemIndex].itemQuantity + 1 };

        const newCartItems = [...cartItems];
        newCartItems[currentMenuItemIndex] = updatedMenuItem;
        setCartItems(newCartItems);

    }
    const handleQuantityDecrease = (id) => {
        const currentMenuItemIndex = cartItems.findIndex((item) => item.menuItem.menuItemId === id);
        if (cartItems[currentMenuItemIndex].itemQuantity === 1) {
            setCartItems(cartItems.filter((item) => item.menuItem.menuItemId !== id))
        } else {
            const updatedMenuItem = { ...cartItems[currentMenuItemIndex], itemQuantity: cartItems[currentMenuItemIndex].itemQuantity - 1 };
            const newCartItems = [...cartItems];
            newCartItems[currentMenuItemIndex] = updatedMenuItem;
            setCartItems(newCartItems);
        }
    }
    return (
        <div className='container my-2' >
            <div style={{ display: "flex", justifyContent: "space-between" }} className="my-1">
                <div style={{ maxWidth: "53%" }}>{props.cartItem.menuItem.menuItemName}</div>
                <div>
                    <BsDashSquareDotted className='mx-2' onClick={()=>{ handleQuantityDecrease(props.cartItem.menuItem.menuItemId) }} />
                    {props.cartItem.itemQuantity}
                    <BsPlusSquareDotted className='mx-2' onClick={()=>{ handleQuantityIncrease(props.cartItem.menuItem.menuItemId) }} />
                    &nbsp; ₹ {props.cartItem.menuItem.menuItemPrice * props.cartItem.itemQuantity}</div>
            </div>
        </div>
    )
}

export default CartItemTemplate
