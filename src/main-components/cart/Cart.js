import React, { useContext } from 'react'
import { BsCart4 } from 'react-icons/bs';
import { CartContext } from '../../context/CartContext';
import CartItemTemplate from './CartItemTemplate';
import cartEmptyImage from "./preview.png";

const Cart = () => {

    const cartCon = useContext(CartContext);
    const { cartItems } = cartCon
    const noOfItemsInCart = cartItems.length;

    


    const billingCSS = {
        position: "absolute",
        bottom: "0%",
        display: "contents"

    }

    const totalAmount = () => {
        let totalAmount = 0;
        cartItems.forEach(cartItem => {
             totalAmount += (cartItem.menuItem.menuItemPrice * cartItem.itemQuantity);
        });
        return totalAmount
    }
    
    return (
        <div className="container" style={{ border: "solid 1px", borderRadius: "12px", minHeight: "400px", position: "relative" }}>
            <div className="my-4 mx-4" style={{ display: "flex", justifyContent: "space-between" }}>
                <h2>Cart </h2><h2 >{noOfItemsInCart}&nbsp;<BsCart4 style={{ marginTop: "-10px" }} /> </h2>
            </div>
            <hr />
            {cartItems.length !== 0 ? <div style={{  minHeight: "280px"}}>
                <div>{cartItems.map((cartItem) => { return <CartItemTemplate key={cartItem.menuItem.menuItemId} cartItem={cartItem} /> })}</div>
            </div>: <img src={cartEmptyImage} alt="cart is empty." style={{maxWidth:"100%"}} /> }
            <div className='container' style={billingCSS}>
                <hr />
                <div style={{ display: "flex", justifyContent: "space-between", margin: "0px 2%" }}>
                    <h3>Total Amount</h3><h3>₹ {totalAmount()}</h3></div>
                <hr />
            </div>
        </div>
    )
}

export default Cart
