import { Rating } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import restaurantContext from '../../context/RestaurantContext';
import MenuList from '../menu/MenuList';
import Overview from '../Overview';

import ImageSlider from './ImageSlider';

const RestaurantPage = (props) => {
    const restaurantsCon = useContext(restaurantContext);
    const { getRestaurantsById,getMenuItems, menuItems } = restaurantsCon

    const params = useParams();

    const images = props.restaurant === null ? [] : props.restaurant.restaurantImages;

    const [slides, setSlides] = useState(images);

    useEffect(() => {
        fetchData();
        // eslint-disable-next-line
    }, [])

    const fetchData = async () => {
        const restro = await getRestaurantsById(params.id)
        setSlides(restro.restaurantImages);
        await getMenuItems(params.id);
    }

    const containerStyles = {
        width: "82%",
        height: "500px",
        margin: "0 auto",
    };

    const restroDetails = {
        display: "flex",
        justifyContent: "space-between"
    }

    return (
        <div>
            {props.restaurant !== null && <div className='container my-3'>
                <div style={containerStyles} className="my-3">
                    <ImageSlider slides={slides} />
                </div>
                <div style={{ marginTop: "40px" }}>
                    <div style={restroDetails}>
                        <h1 className="mx-3">{props.restaurant.restaurantName}</h1>
                        <Rating className="mx-3" name="read-only" value={props.restaurant.description.rating} readOnly style={{ float: "right", marginTop: "10px" }} /></div>
                    <div className="card-text my-1 mx-3" style={restroDetails}>
                        <h4>Timing : {props.restaurant.description.timing}</h4>
                        <h4>Cost For Two : {props.restaurant.description.costForTwo}</h4>
                    </div>
                    <div className="my-2 mx-3"><h4>{props.restaurant.description.address}</h4></div>
                    <hr />
                </div>
                <div className='my-5'>
                    <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist" style={{ justifyContent: "center" }}>
                        <li className="nav-item mx-1" role="presentation">
                            <button className="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Order Online</button>
                        </li>
                        <li className="nav-item mx-1" role="presentation">
                            <button className="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Overview</button>
                        </li>
                    </ul>
                    <div className="tab-content" id="pills-tabContent">
                        <div className="tab-pane fade show active my-5" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab"><MenuList restaurant={props.restaurant} menuItems={menuItems} /></div>
                        <div className="tab-pane fade my-5" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab"><Overview restaurant={props.restaurant}/></div>
                    </div>
                </div>
            </div>}
        </div>
    )
}

export default RestaurantPage 
