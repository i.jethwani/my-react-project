import React from 'react'
import Cart from '../cart/Cart'
import MenuItem from './MenuItem'

const MenuList = (props) => {

    return (
        <div className="container">
            {props.menuItems !== null && <div className='row'>
                <div className='col'>
                    {props.restaurant.cuisines.map((cuisine) => {
                        return <div className="accordion" id="accordionExample" key={cuisine.cuisineId}>
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingOne">
                                    <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h4>{cuisine.name}</h4>
                                    </button>
                                </h2>
                                <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div className="accordion-body">
                                        {props.menuItems.map((menuItem) => {
                                            return <div key={menuItem.menuItemId}>
                                                {menuItem.cuisine.name === cuisine.name && <MenuItem menuItem={menuItem} />}
                                            </div>
                                        })
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    })}
                </div>
                <div className='col-md-4'>
                    <Cart />
                </div>
            </div>}
        </div>
    )
}

export default MenuList
