import React, { useContext, useRef, useState } from 'react'
import veg from '../../shared-components/veg.png'
import nonVeg from '../../shared-components/nonVeg.png'
import { CartContext } from '../../context/CartContext';
import Modal from '../../shared-components/Modal';

const MenuItem = (props) => {

    const cartCon = useContext(CartContext);
    const { cartItems, setCartItems } = cartCon

    const ref = useRef(null);
    const [showModal,setShowModal] = useState(false);

    const handleOnAddToCart = async (menuItem) => {
        let addItem = true;

        if (cartItems.length !== 0) {
            if (cartItems[0].menuItem.restaurant.restaurantId !== menuItem.restaurant.restaurantId) {
                await setShowModal(true);
                ref.current.click();
            }

            cartItems.map((item) => {
                if (item.menuItem.menuItemId === menuItem.menuItemId) {   
                    addItem=false;                 
                    const currentMenuItemIndex = cartItems.findIndex((item) => item.menuItem.menuItemId === menuItem.menuItemId);
                    const updatedMenuItem = { ...cartItems[currentMenuItemIndex], itemQuantity: cartItems[currentMenuItemIndex].itemQuantity + 1 };

                    const newCartItems = [...cartItems];
                    newCartItems[currentMenuItemIndex] = updatedMenuItem;
                    return setCartItems(newCartItems);
                }
                else {
                    return null;
                }
            })
        }

        if (addItem) {
            await setCartItems(cartItems.concat({ menuItem: menuItem, customer: null, itemQuantity: 1 }));            
        }
    }

    return (
        <div className='container my-3' style={{ border: "solid 1px", borderRadius: "12px" }}>
            {showModal && <Modal ref={ref} menuItem={props.menuItem} restaurantId={cartItems[0].menuItem.restaurant.restaurantId} />}
            <div style={{ display: "flex", justifyContent: "space-between" }} className="my-2">
                <h4>{props.menuItem.menuItemName}</h4> {props.menuItem.isVeg ? <img src={veg} alt="veg" style={{ width: "5%" }} /> : <img src={nonVeg} alt="nonVeg" style={{ width: "5%" }} />}
            </div>
            <div style={{ display: "flex", justifyContent: "space-between" }} className="my-2">
                <h5>₹ {props.menuItem.menuItemPrice}</h5> <button onClick={() => { handleOnAddToCart(props.menuItem) }} className="btn btn-outline-primary btn-sm">Add</button>
            </div>
        </div>
    )
}

export default MenuItem
