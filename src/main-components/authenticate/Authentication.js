import React, { Component } from 'react';
import { NavLink } from "react-router-dom";

import "./Authentication.css";


export class Authentication extends Component {
  render() {
    return (
        <div className="App my-3">
          <div className="appAside" />
          <div className="appForm">
            <div className="pageSwitcher">
              <NavLink to="/sign-in" activeclassname="pageSwitcherItem-active" className="pageSwitcherItem">
                Sign In
              </NavLink>
              <NavLink exact to="/sign-up" activeclassname="pageSwitcherItem-active" className="pageSwitcherItem">
                Sign Up
              </NavLink>
            </div>

            <div className="formTitle">
              <NavLink to="/sign-in" activeclassname="formTitleLink-active" className="formTitleLink">
                Sign In
              </NavLink>{" "}
              or{" "}
              <NavLink exact to="/sign-up" activeclassname="formTitleLink-active" className="formTitleLink">
                Sign Up
              </NavLink>
            </div>
          </div>
        </div>
    )
  }
}

export default Authentication
