import { Rating } from '@mui/material';
import React, { useContext, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import restaurantContext from '../../context/RestaurantContext'
import './homePage.css'

const RestaurantList = () => {
  const restaurantsCon = useContext(restaurantContext);
  const { restaurants, getRestaurants, getRestaurantsById } = restaurantsCon;
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      await getRestaurants()
    }
    fetchData()
    // eslint-disable-next-line
  }, [])

  const handleOnClick = async(id,event) => {
    event.preventDefault();
    await getRestaurantsById(id);
    navigate(`restaurant/${id}`);
  }

  return (
    <div className='container row my-5 mx-1' style={{ backgroundColor: "lightGrey", borderRadius: "15px" }}>
      <p className='font'>Order food online in Kamla Mills, Lower Parel</p>
      {restaurants.map((restro) => {
        return <div className="card col-md-3 mx-4 my-4" style={{ width: "370px",cursor:"pointer"}} key={restro.restaurantId} onClick={(event)=>{handleOnClick(restro.restaurantId,event)}}  >
          <div style={{ height: "265px" }}><img src={restro.restaurantImages[0].imageUrl} style={{ maxHeight: "250px" }} className="card-img-top my-2" alt="..." /></div>
          <div className="card-body" >
            <h3 className="card-title">{restro.restaurantName}</h3>
            <hr />
            <Rating name="read-only" value={restro.description.rating} readOnly style={{ float: "right" }} />
            <div className="card-text my-1">{restro.description.timing}</div>
            <div className="my-1">Cost For Two : {restro.description.costForTwo}</div>
            <hr />
            <div className="my-1">{restro.description.address}</div>
          </div>
        </div>
      })}
    </div>
  )
}

export default RestaurantList
