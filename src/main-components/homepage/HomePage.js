import React from 'react'
import './homePage.css'
import RestaurantList from './RestaurantList'

const HomePage = () => {
    const dishImages = [{
        url: 'https://b.zmtcdn.com/data/dish_images/197987b7ebcd1ee08f8c25ea4e77e20f1634731334.png',
        imageName: 'Chicken wings',
        restaurantId: 1,
      },
      {
        url: 'https://b.zmtcdn.com/data/dish_images/838c7929dcc09479600f118c9088af7b1614910398.png',
        imageName: 'Rice Bowl',
        restaurantId: 1,
      },
      {
        url: 'https://b.zmtcdn.com/data/dish_images/c2f22c42f7ba90d81440a88449f4e5891634806087.png',
        imageName: 'Rolls',
        restaurantId: 1,
      },
      {
        url: 'https://b.zmtcdn.com/data/o2_assets/52eb9796bb9bcf0eba64c643349e97211634401116.png',
        imageName: 'Thali',
        restaurantId: 1,
      },
      {
        url: 'https://b.zmtcdn.com/data/o2_assets/fc641efbb73b10484257f295ef0b9b981634401116.png',
        imageName: 'SandWich',
        restaurantId: 1,
      },
      {
        url: 'https://b.zmtcdn.com/data/dish_images/ccb7dc2ba2b054419f805da7f05704471634886169.png',
        imageName: 'Burger',
        restaurantId: 1,
      }]
  return (
    <div>
      <div className='container my-3' style={{backgroundColor:"lightGrey",borderRadius:"15px"}}>
        <div><p className='font'>Inspiration for your first order ....</p></div>
        <div>{dishImages.map((img)=>{
           return <img src={img.url} key={img.imageName} className="mx-1 my-2" style={{borderRadius:"50%",height: "190px",width:" 200px",cursor:"pointer"}} alt="advertsing food images" />   
        })}</div>        
        <br />
      </div>
      <div>
        <RestaurantList/>
      </div>
    </div>
  )
}

export default HomePage
