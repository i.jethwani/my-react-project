import React, { useState } from 'react'
import restaurantContext from './RestaurantContext';

const RestaurantState = (props) => {

    const [restaurants,setRestaurants] = useState([]);
    const [restaurant,setRestaurant]=useState(null);
    const [menuItems,setMenuItems]=useState(null);

    const getRestaurants = async () => {
        // API Call 
        const response = await fetch(`http://localhost:8080/restaurants`, {
            method: 'GET'            
        });
        const json = await response.json();
        setRestaurants(json);
    }

    const getRestaurantsById = async (id) => {
        // API Call 
        const response = await fetch(`http://localhost:8080/restaurants/${id}`, {
            method: 'GET'            
        });
        const json = await response.json();
        setRestaurant(json);
        return json;
    }

    const getMenuItems = async (id) =>{
        const response = await fetch(`http://localhost:8080/menuItems/${id}`, {
            method: 'GET'            
        });
        const json = await response.json();
        setMenuItems(json);
    }


    return (
        <restaurantContext.Provider value={{ restaurants, getRestaurants,getRestaurantsById,restaurant,getMenuItems,menuItems}}>
            {props.children}
        </restaurantContext.Provider>
    );
}

export default RestaurantState
