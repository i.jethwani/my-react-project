import React,{ createContext, useState } from "react";

export const CartContext = createContext();

export const CartState = (props) => {

    const [cartItems,setCartItems] = useState([]);

  return (
    <div>
        <CartContext.Provider value={{cartItems,setCartItems}}>
            {props.children}
        </CartContext.Provider>
    </div>
  )
}